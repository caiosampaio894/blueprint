from flask import Flask, request, jsonify


def init_app(app: Flask):
   from blueprint.views.animes import bp_animes
   app.register_blueprint(bp_animes)