from flask import Blueprint, request, jsonify
from blueprint.services import Anime
from blueprint.views.exc import EmptyList, KeyUnavailable, UserNotFoundError
from psycopg2.errors import UniqueViolation

bp_animes = Blueprint('animes', __name__, url_prefix='/animes')

@bp_animes.route('', methods=['POST'])
def get_create():
    data = request.json

    try:
        data['anime'] = data['anime'].title()
        anime = Anime(data)
        new_anime = anime.save(data)
        return new_anime, 201
    except UniqueViolation as e:
        return {'message': 'anime already exists'}, 422
    # except KeyUnavailable as e:
    #     return {'message': str(e)}, 422


@bp_animes.route('', methods=['GET'])
def get_create_2():
    try:
        anime_list = Anime.get_all()
        return jsonify(anime_list), 200
    except EmptyList as e:
        return {'message': str(e)}

@bp_animes.route('/<int:anime_id>', methods=['GET'])
def filter(anime_id: int):
    data = Anime.get_by_id(anime_id)
    if data:
        return data, 200
    else:
        return {'data': []}, 404

@bp_animes.route('/<int:anime_id>', methods=['PATCH'])
def update(anime_id: int):
    try:
        data = request.json
        update_anime = Anime.update(anime_id, data)
        return update_anime, 200
    except UserNotFoundError as e:
        return {'message': str(e)}, 404

@bp_animes.route('/<int:anime_id>', methods=['DELETE'])
def delete(anime_id:int):
    try:
        deleted_anime = Anime.delete(anime_id)
        return deleted_anime, 204
    except UserNotFoundError as e:
        return {'message': str(e)}, 404

