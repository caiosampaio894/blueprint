

class UserNotFoundError(Exception):
    ...

class EmptyList(Exception):
    ...

class KeyUnavailable(Exception):
    ...