from re import S
import psycopg2
from psycopg2 import sql
from dotenv import load_dotenv
import os
from datetime import date
from typing import Union
from blueprint.views.exc import KeyUnavailable, UserNotFoundError

load_dotenv()


configs = {
    'host': os.environ.get('DB_HOST'),
    'database': os.environ.get('DB_NAME'),
    'user': os.environ.get('DB_USER'),
    'password': os.environ.get('DB_PWD') 
}



def create_table():
    conn = psycopg2.connect(**configs)

    cur = conn.cursor()

    cur.execute("""  
        create table if not exists anime(
    	id bigserial        primary key,
	    anime               varchar(100) not null unique, 
	    released_date       date not null,
	    seasons             integer not null
);
    """)

    conn.commit()
    cur.close()
    conn.close()
    

class Anime():

    def __init__(self, fields: Union[tuple, dict]) -> None:
        if type(fields) is tuple:
            self.id, self.anime, self.released_date, self.seasons = fields

        if type(fields) is dict:
            for k,v in fields.items():
                setattr(self, k, v)

    
    @staticmethod
    def get_all():
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        cur.execute('select * from anime;')

        result = cur.fetchall()

        conn.commit()
        cur.close()
        conn.close()

        serialized_data = [Anime(anime_data).__dict__ for anime_data in result]

        return serialized_data  


    @staticmethod
    def get_by_id(id):
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        cur.execute('select * from anime where id=(%s);', (id, ))

        result = cur.fetchone()

        conn.commit()
        cur.close()
        conn.close()

        serialized_data = Anime(result).__dict__ 

        return serialized_data  
    
    def save(self, data):
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()
        create_table()

        columns = [sql.Identifier(key) for key in self.__dict__.keys()]
        values = [sql.Literal(value) for value in self.__dict__.values()]

        # for key in data:
        #     if key not in columns:
        #         raise KeyUnavailable(f"""
        #         'available keys': ['anime', 'released_date', 'seasons'], 'wrong_keys': [{key}]
        #         """)


        query = sql.SQL(
            """
                insert into
                    anime(id, {columns})
                values
                    (default, {values})
                returning *
            """).format(columns=sql.SQL(',').join(columns),
                        values=sql.SQL(',').join(values))

        print(query.as_string(cur))
        
        cur.execute(query)

        fetch_result = cur.fetchone()
      
        conn.commit()
        cur.close()
        conn.close()
        
        serialized_data = Anime(fetch_result).__dict__

        return serialized_data

    @staticmethod
    def update(id:int, data):
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        columns =  [sql.Identifier(key) for key in data.keys()]
        values = [sql.Literal(value) for value in data.values()]
        
        query = sql.SQL(
            """
                update
                    anime
                set
                    ({columns}) = row({values})
                where
                    id={id}
                returning *
            """).format(id=sql.Literal(str(id)), 
                    columns=sql.SQL(',').join(columns), 
                    values=sql.SQL(',').join(values))
        
        cur.execute(query)

        result = cur.fetchone()

        conn.commit()
        cur.close()
        conn.close()

        if not result:
            raise UserNotFoundError(f'Usuário com id {id} não encontrado.')
            
        serialized_data = Anime(result).__dict__

        return serialized_data


    @staticmethod
    def delete(id):
        
        conn = psycopg2.connect(**configs)
        cur = conn.cursor()

        cur.execute(""" delete from
                            anime
                        where
                            id=(%s)
                        returning *;""", (id, ))

        fetch_result = cur.fetchone()
      
        conn.commit()
        cur.close()
        conn.close()

        if not fetch_result:
            raise UserNotFoundError(f'Usuário com id {id} não encontrado.')
        
        serialized_data = Anime(fetch_result).__dict__

        return serialized_data
